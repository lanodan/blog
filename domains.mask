# Facebook
facebook.com
facebook.net
instagram.com

# Google // used their TLS certificate and modified it by hand
android.com
g.co
goo.gl
google-analytics.com
google.ca
google.cl
google.co.in
google.co.jp
google.co.uk
google.com
google.com.ar
google.com.au
google.com.br
google.com.co
google.com.mx
google.com.tr
google.com.vn
google.de
google.es
google.fr
google.hu
google.it
google.nl
google.pl
google.pt
googleadapis.com
googleapis.cn
googlecommerce.com
googlevideo.com
gstatic.cn
gstatic.com
gvt1.com
gvt2.com
metric.gstatic.com
urchin.com
url.google.com
www.goo.gl
youtu.be
youtube-nocookie.com
youtube.com
youtubeeducation.com
ytimg.com
ampproject.org

# Amazon
amazon.com
cloudfront.net

# Privacy
solvemedia.com
gravatar.com

# Tracker-service
mailchimp.com
mandrill.com
tinyletter.com
list-manage.com

# Useless shit
wikiwand.com

# Pay-wall
presseocean.fr

# Hackers for GAFAMs
hackerone.com

# Cookie-wall and not-trustable anymore
# http://arstechnica.com/information-technology/2015/05/sourceforge-grabs-gimp-for-windows-account-wraps-installer-in-bundle-pushing-adware/
fsdn.org
slashdotmedia.com
slashdot.org
sourceforge.org

# Terms/cookie-wall
curiouscat.me

# Not trustable
# https://twitter.com/lanodan/status/769442479020072961
# https://en.wikipedia.org/wiki/TRUSTe
truste.com
truste.org

# Sexist and LGBT-hating
doublehop.me

# Trump shit
ilovevitaly.com
ɢoogle.com

# Unuseable without too-much whitelists
medium.com

# Social-Intranetwork
dlvr.it

# TOXIC
jeuxvideo.com
webedia.fr

# Fuckware (define that for yourself)
01net.com

# Transphobie / pas-safe
fr.wikipedia.org

# Les Survivants (anti-avortement)
lessurvivants.fr
lescharlatans.fr
simoneveil.com

# “Let’s make web shit again” — AMP
amp.*
amproject.org

# GDPR-Wall
tumblr.com
