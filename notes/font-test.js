// Copyright 2023 Haelwenn (lanodan) Monnier <contact+blog@hacktivis.me>
// SPDX-License-Identifier: MIT

'use strict';

const main = document.getElementsByTagName("main")[0];

main.style.fontSynthesis = 'none';

var fontfamilies = "";
[
	"system-ui", "serif", "sans-serif", "monospace", "cursive", "fantasy", "ui-serif", "ui-sans-serif", "ui-monospace", "ui-rounded", "math", "emoji", "fangsong"
].forEach(function(e) {fontfamilies += `\t\t<option>${e}</option>\n`});

var fontweights = "";
[
	"100", "200", "300", "400", "500", "600", "700", "800", "900", "950"
].forEach(function(e) {fontweights += `\t\t<option>${e}</option>\n`});

var fontstyles = "";
[
	"normal", "italic", "oblique", "oblique -14deg", "oblique 90deg", "oblique -90deg"
].forEach(function(e) {fontstyles += `\t\t<option>${e}</option>\n`});

document.getElementById('fontsel').innerHTML = `
	<label>Family:
		<input id="fontsel_in" type="text" list="fontfamilies"/>
		<datalist id="fontfamilies">${fontfamilies}</datalist>
	</label>
	<label>Weight (<span id="fontsel_weight_val">400</span>):
		<input id="fontsel_weight" type="range" max="1000" min="1" step="50" value="400" list="fontweights"/>
		<datalist id="fontweights">${fontweights}</datalist>
	</label>
	<label>Style:
		<input id="fontsel_style" type="text" list="fontstyles"/>
		<datalist id="fontstyles">${fontstyles}</datalist>
	</label>
`;

document.getElementById('fontsel_in').addEventListener('change', (e) => {
	main.style.fontFamily = e.target.value;
});

main.style.fontWeight = 400; // Normal

document.getElementById('fontsel_weight').addEventListener('change', (e) => {
	main.style.fontWeight = e.target.value;
	document.getElementById('fontsel_weight_val').innerText = main.style.fontWeight;
});

document.getElementById('fontsel_style').addEventListener('change', (e) => {
	main.style.fontStyle = e.target.value;
});
