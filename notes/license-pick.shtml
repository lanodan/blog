<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
<!--#include file="/templates/head.shtml" -->
		<title>Picking a License — lanodan’s cyber-home</title>
	</head>
	<body>
<!--#include file="/templates/en/nav.shtml" -->
	<main>
		<h1>Picking a License</h1>
		<p>Largely copied from: <a href="gemini://drewdevault.com/2021/05/20/How-I-choose-a-license.gmi">Drew DeVault - How I choose a license</a></p>
		<p>Similarly: "This is how I choose a license for a new project. It is a reflection of my values and priorities and may not work for your needs."</p>

		<section id="matters">
			<h2>matters</h2>
			<dd>
				<p>Does it matters at all? Is it even copyrightable? (ie. simple observations without originality)</p>
				<dl>
					<dt>Yes</dt><dd><a href="#software">Go to: software</a></dd>
					<dt>No</dt><dd>Use CC0</dd>
				</dl>
				<p>Note: CC0 is one of the rare licences that trully waives as much copyrights as possible, even in jurisdictions where Public Domain cannot simply be assigned to (like in Europe).</p>
			</dd>
		</section>

		<section id="software">
			<h2>software</h2>
			<dd>
				<p>Is it software?</p>
				<dl>
					<dt>Yes</dt><dd><a href="#ubiquitous">Go to: ubiquitous</a></dd>
					<dt>No</dt><dd><a href="#creative">Go to: creative</a></dd>
				</dl>
			</dd>
		</section>

		<section id="creative">
			<h2>creative</h2>
			<dd>
				<p>See <a href="https://creativecommons.org/choose/">Creative Commons - Choose a License</a>, incomplete version below</p>
				<p>Do you want it to become ubiquitous, where anyone, including corporations, governments, literally anyone, would have no reservations about using it for any use-case, including making proprietary derivatives and selling them, or reusing the code in another project, proprietary or not?</p>
				<dl>
					<dt>Yes</dt><dd>Use CC-BY-4.0</dd>
					<dt>No, libre-only</dt><dd>Use CC-BY-SA-4.0</dd>
					<dt>No, non-commercial</dt><dd>Use CC-BY-NC-SA-4.0</dd>
				</dl>
			</dd>
		</section>

		<section id="ubiquitous">
			<h2>ubiquitous</h2>
			<dd>
				<p>Do you want it to become ubiquitous, where anyone, including corporations, governments, literally anyone, would have no reservations about using it for any use-case, including making proprietary derivatives and selling them, or reusing the code in another project, proprietary or not?</p>
				<dl>
					<dt>Yes</dt><dd><a href="#patents">Go to: patents</a></dd>
					<dt>No</dt><dd><a href="#service">Go to: service</a></dd>
				</dl>
			</dd>
		</section>

		<section id="patents">
			<h2>patents</h2>
			<dd>
				<p>Is the copyright owned by a company who might have trademarks and patents and other such garbage?</p>
				<dl>
					<dt>Yes</dt><dd>Use Apache 2.0</dd>
					<dt>No</dt><dd>Use MIT or BSD</dd>
				</dl>
			</dd>
		</section>

		<section id="service">
			<h2>service</h2>
			<dd>
				<p>Is it a networked service or similar software not entirely running directly on the user's machine?</p>
				<dl>
					<dt>Yes</dt><dd>Use AGPLv3</dd>
					<dt>No</dt><dd><a href="#library">Go to: library</a></dd>
				</dl>
			</dd>
		</section>

		<section id="library">
			<h2>library</h2>
			<dd>
				<p>Is it a library?</p>
				<dl>
					<dt>Yes</dt><dd><a href="#vendoring">Go to: vendoring</a></dd>
					<dt>No</dt><dd>Use <code>GPL-2.0-only OR GPL-3.0-only</code> (because of incompatibilities between both…)</dd>
				</dl>
			</dd>
		</section>

		<section id="vendoring">
			<h2>vendoring</h2>
			<dd>
				<p>Do you want users to be able to vendor it (copy it into their code), or should they be required to install it and link to it to use it without the viral obligation?</p>
				<dl>
					<dt>Yes</dt><dd>Use MPL 2.0</dd>
					<dt>No</dt><dd>Use LGPLv3</dd>
				</dl>
			</dd>
		</section>
	</main>
<!--#include file="/templates/en/footer.shtml" -->
	</body>
</html>
