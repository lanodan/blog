# SSH Server comparison of protocol and cryptography
Note: Done by using the source code and listing everything that is available, regardless of the build or runtime configurations.

- tinyssh: Minimalist, state-of-the-art cryptographic and secure-only SSH Daemon <https://tinyssh.org/>
- dropbear: Small SSH Client/Server designed for small memory usage <https://matt.ucc.asn.au/dropbear/dropbear.html>
- OpenSSH: Bloated SSH Suite from OpenBSD <https://openssh.org/>

## Versions
- tinyssh: SSH2
- dropbear: SSH2
- OpenSSH: SSH1, SSH2

## Key Exchange
- tinyssh (20190101): curve25519-sha256, curve25519-sha256@libssh.org, sntrup4591761x25519-sha512@tinyssh.org
- tinyssh (20181206): curve25519-sha256, curve25519-sha256@libssh.org, ecdh-sha2-nistp256
- dropbear (2018.76): curve25519-sha256, curve25519-sha256@libssh.org, ecdh-sha2-nistp521, ecdh-sha2-nistp384, ecdh-sha2-nistp256, diffie-hellman-group14-sha256, diffie-hellman-group14-sha1, diffie-hellman-group1-sha1, diffie-hellman-group16-sha512
- libssh-based: curve25519-sha256, ecdh-sha2-nistp256, diffie-hellman-group1-sha1, diffie-hellman-group14-sha1

## Ciphers
- tinyssh (20190101): chacha20-poly1305@openssh.com
- tinyssh (20181206): chacha20-poly1305@openssh.com, aes256-ctr
- dropbear (2018.76): aes128-ctr, aes256-ctr, twofish256-ctr, twofish128-ctr, aes128-cbc, aes256-cbc, twofish256-cbc, twofish-cbc, twofish128-cbc, 3des-ctr, 3des-cbc, blowfish-cbc
- libssh-based: chachae20-poly1305, aes256-ctr, aes192-ctr, aes128-ctr, aes256-cbc, aes192-cbc, aes128-cbc, 3des-cbc, blowfish-cbc

## HMAC
- tinyssh (20190101): hmac-sha2-256
- tinyssh (20181206): hmac-sha2-256
- dropbear (2018.76): hmac-sha1-96, hmac-sha1, hmac-sha2-256, hmac-sha2-512, hmac-md5
- libssh-based: hmac-sha2-512, hmac-sha2-256, hmac-sha1, none

## Compression
- tinyssh (20190101): none
- tinyssh (20181206): none
- dropbear (2018.76): zlib@openssh.com, zlib, none
- libssh-based: zlib, zlib@openssh.com, none

## Host Keys
- tinyssh (20190101): ssh-ed25519
- tinyssh (20181206): ssh-ed25519, ecdsa-sha2-nistp256
- dropbear (2018.76): ssh-rsa, ssh-dss, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521
- libssh-based: ssh-ed25519, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-rsa, rsa-sha2-512, rsa-sha2-256, ssh-dss

## Client Keys
- tinyssh (20190101): ssh-ed25519
- tinyssh (20181206): ssh-ed25519, ecdsa-sha2-nistp256
- dropbear (2018.76): ssh-rsa, ssh-dss, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521
- libssh-based: ssh-ed25519, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-rsa, rsa-sha2-512, rsa-sha2-256, ssh-dss

## Authentication
- libssh-based: none, password, public-key, keyboard-interactive, gssapi-with-mic

## Channels
- libssh-based: shell, exec, direct-tcpip, subsystem, auth-agent-req@openssh.com
