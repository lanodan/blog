# Time units
minute=60
hour=60*minute
day=24*hour
week=7*day
year=365.24219879*day
month=year/12
# SI Units
yocto=10^(-24)
zepto=10^(-21)
atto=10^(-18)
femto=10^(-15)
pico=10^(-12)
nano=10^(-9)
micro=10^(-6)
milli=10^(-3)
centi=10^(-2)
deci=10^(-1)
deca=10^1
hecto=10^2
kilo=10^3
mega=10^6
giga=10^9
tera=10^12
peta=10^15
exa=10^18
zetta=10^21
yotta=10^24
