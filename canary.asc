-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

This is an OpenPGP signed canary. The first attempt to do one failed… (I’m bad at having habits)
I choosed OpenPGP because it allows to:
• verify my identity (web of trust, meeting me, …)
• have a crypto-signed: text, date, expiration date
• make more use of good cryptography

This canary will be updated around the end of each month if not needed to be earlier, cryptographicaly expires after 2 months.

## Why a canary?
I originally started it for fun, but I ended up:
• Hosting my own software, which is a good thing btw. https://hacktivis.me/git/
• Being a proxy-maintainer for few gentoo packages
• Hosting my own social-network instance

And I will probably have more involvements in the future and I think it's better and or even mandatory to have a canary in thoses cases.

## Statements
• anything that can harm and/or have harmed data, received or send to me and or my machines:
        • SSID=orange, modifies few paquets, got fixed few hours later, no harmful data sent.
                • DNS gave a weird answer, fixed directly.[1]
                • SMTPS doesn’t works, SASL fails and certificate doesn’t match
• no request from any law agency (warrants, court order)
• no police raid or equivalent
• no request for backdoors or equivalent
• my disks for anything but datalove are encrypted(LUKS)
• my keys are all safe and protected and old ones are revoked and few old ones unuseable(shred)
	• On 2016-12-17 I made myself a new keyring, the old one was a huge mess… sorry for the mess of revoked keys
	• On 2017-05-03 04:19:00 I made new (ECC) keys, after hardware failure and no backups or old keys, and so broke HPKP
	• On 2018-01-11 I changed my default OpenPGP key set to DDC9237C14CF6F4DD847F6B390D93ACCFEFF61AE and it’s now store only on a Nitrokey Start and a paperkey backup [2]

## Recent News
• Some at the EU Parliement said that the signers of https://www.change.org/p/european-parliament-stop-the-censorship-machinery-save-the-internet aren’t real (it’s on change.org so I didn’t sign it btw) https://mamot.fr/users/doctorow/statuses/100599594052073583
• IRC turned 30 years old. http://www.oulu.fi/university/node/54247
• Landline Phones will be turned off in France if you use Orange(with bought the Historical Nationnal Phone Operator) http://www.leparisien.fr/economie/le-telephone-fixe-c-est-bientot-fini-25-08-2018-7864455.php

## Commands used
• cp canary.asc canary
• vis canary
• gpg --default-sig-expire 2m --clearsign canary
• shred -u canary

### TL;DR
• I’m safe but you should not have a blind trust on me.

- - -- 
Inspired by: https://fyb.patternsinthevoid.net/canary.asc and https://github.com/QubesOS/qubes-secpack/blob/master/canaries

1: https://hacktivis.me/articles/La%20neutralit%C3%A9e%20du%20Net%20sur%20un%20wifi%20Orange%E2%84%A2,%20deuxi%C3%A8me%20mensonge
2: https://hacktivis.me/articles/I%20changed%20my%20OpenPGP%20keys
-----BEGIN PGP SIGNATURE-----

iHsEARYKACMWIQT4W9xj/ZtK9Ev2uBLVt6jkPJl97gUCW4Ll1QWDAE8aAAAKCRDV
t6jkPJl97qucAQCFw+YVoI3QzhijlffZgiB5hFN+jH8FDqeLOiOBAiRhcwEA/rnC
+xl4agRXlTjPE7tPwwyaK1y9kQ8N9EQDd5dYPgE=
=JRvf
-----END PGP SIGNATURE-----
